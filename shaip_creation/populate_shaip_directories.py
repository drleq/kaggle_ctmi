"""
This script should be run once upon installation to setup the required input directories.
Input:  ./dicom_dir/*.dcm
Outputs: ./shaip/workspace/input/data
         ./shaip/workspace/input/ground_truth
Each of the data and ground_truth directories will have a folder per dataset, named by the
dataset id - e.g. ID_0001, containing the dicom file and GT respectively.

"""

import os
import shutil
from glob import glob


def _filename_to_contrast_gtstring(fname):
    """ Filenames look like this: "ID_0087_AGE_0044_CONTRAST_0_CT.dcm """
    assert fname[17:25] == 'CONTRAST'
    c = fname[26]
    assert c in ('0', '1')
    return 'ct' if c == '0' else 'cta'


def _filename_to_id(fname):
    """ Filenames look like this: "ID_0087_AGE_0044_CONTRAST_0_CT.dcm
    Return a string containing only the digits"""
    assert fname[:3] == 'ID_'
    return '0' + fname[3:7]


def do_it(shaip_root, only_these_ids=None):
    dicom_dir = 'dicom_dir'
    shaip_input_dir = os.path.join(shaip_root, 'input')
    gt_dir = os.path.join(shaip_root, 'input/ground_truth')
    data_dir = os.path.join(shaip_root, 'input/dicom')

    if not os.path.isdir(dicom_dir):
        print("Source data directory %s not found", dicom_dir)
        print("Download data from %s",
              'https://www.kaggle.com/kmader/siim-medical-images/version/6#dicom_dir.zip')
        assert False

    os.makedirs(shaip_input_dir, exist_ok=True)

    assert not os.path.exists(gt_dir)
    assert not os.path.exists(data_dir)

    # Create the directories
    os.makedirs(gt_dir)
    os.makedirs(data_dir)

    # Glob the datasets
    datasets_paths = glob('dicom_dir/*.dcm')
    datasets_paths.sort()
    assert len(datasets_paths) == 100

    # Iterate of datasets, creating the folder in data and groundtruth
    # then copying in the data and a simple gt file - content 'ct' or 'cta'
    for dpath in datasets_paths:
        print("Processing", dpath)
        _, dname = os.path.split(dpath)
        id_ = _filename_to_id(dname)
        if (only_these_ids is not None) and (id_ not in only_these_ids):
            print('Skipping...', id_)
            continue
        gt = _filename_to_contrast_gtstring(dname)
        this_gt_dir = os.path.join(gt_dir, 'study' + id_ + '/series00000/')
        this_data_dir = os.path.join(data_dir, 'study' + id_ + '/series00000/')

        assert not os.path.exists(this_gt_dir)
        assert not os.path.exists(this_data_dir)

        this_gt_path = os.path.join(this_gt_dir, id_ + '.txt')
        this_data_path = os.path.join(this_data_dir, id_ + '.dcm')

        print(this_gt_path)
        print(this_data_path)
        print()

        os.makedirs(this_gt_dir)
        os.makedirs(this_data_dir)

        shutil.copyfile(dpath, this_data_path)
        with open(this_gt_path, 'w') as f:
            f.write(gt)
            f.write('\n')

        assert os.path.exists(this_gt_path)
        assert os.path.exists(this_data_path)

        # Also create the output and cache directories
        os.makedirs(os.path.join(shaip_root, 'output/results'), exist_ok=True)
        os.makedirs(os.path.join(shaip_root, 'output/models'), exist_ok=True)
        os.makedirs(os.path.join(shaip_root, 'cache'), exist_ok=True)

    print('Done')


if __name__ == '__main__':

    prepare_unittest_workspace = False
    prepare_full_workspace = True

    if prepare_full_workspace:
        # First for the main workspace, with all 100 images
        do_it('workspace')

    if prepare_unittest_workspace:
        # Following only used by Ian to create the ShaipUnittestWorkspace, which is committed.
        # Then for unit tests we select 8 + 8 datasets, balancing ct and cta
        unit_test_ids_1 = ['0000' + str(i) for i in range(8)]
        unit_test_ids_0 = ['0005' + str(i) for i in range(8)]
        unit_test_ids = unit_test_ids_1 + unit_test_ids_0
        print(unit_test_ids)
        assert len(unit_test_ids) == 16

        do_it('unittest/workspace', unit_test_ids)

"""
A maximally simple solution to CT / CTA detection!
"""
import datetime
import logging
import os
from glob import glob

# Set the 'Agg' matplotlib backend to avoid plots appearing on the display (we only want them
# saved to .png files)
import matplotlib
import numpy as np
import pandas as pd
# from IPython.core.display import display
from sklearn.metrics import accuracy_score

matplotlib.use('Agg')

# noinspection PyPep8
from matplotlib import pyplot as plt


# noinspection PyMethodMayBeStatic,PyMethodMayBeStatic,PyMethodMayBeStatic
class Results(object):

    def __init__(self, results_dir):
        self.results_dir = results_dir

    # Constants (will be @patched by unittests for speed)
    show_images_figsize = (12, 12)
    show_images_nrows = 3
    show_images_ncols = 3

    def generate_top_level_index_html(self):
        """ Given filename should include the path and filename. Write the file
        into the SHAIP results_dir directory """
        # noinspection PyPep8

        contents = """<!DOCTYPE html>
    <html>
    <body>
    
    <h1>Kaggle CTMI Example results</h1>
    
    <h2>Date and time of run: %s </h2>
    
    <h2> Introduction </h2>
    These are results for the problem of CT/CTA detection.
   <br>
    Datasets come from the 
    <a href="https://www.kaggle.com/kmader/siim-medical-images/home">
    Kaggle Medical Imaging dataset</a>.  
    The data is originally from  
    <a href="https://wiki.cancerimagingarchive.net/display/Public/TCGA-LUAD">
    The Cancer Genome Atlas LUADA</a> collection
    
    <h2><a href="training_results.html"> Training Results </a></h2>
    
    <h2><a href="evaluation_results.html"> Evaluation Results </a></h2>
    
    <h2><a href="kaggle-ctmi.log"> Log File </a></h2>
    </body>
    </html>
    """
        datetime_string = datetime.datetime.now().strftime("%d-%m-%Y, %H:%M")
        with open(self.results_dir + 'index.html', 'w') as fp:
            fp.write(contents % datetime_string)

    def explore_cohort(self, cohort, predictions=None, savefilename=None):

        if predictions:
            assert all([p in (0, 1) for p in predictions])
            cols = ['ID', 'GT', 'PR', 'Dtype', 'MinV', 'MaxV', 'Slope', 'Incpt', 'MmPerPix',
                    'Padding']
        else:
            cols = ['ID', 'GT', 'Dtype', 'MinV', 'MaxV', 'Slope', 'Incpt', 'MmPerPix',
                    'Padding']
        df = pd.DataFrame(
            columns=cols)

        for ix in range(cohort.size):
            image = cohort.images[ix]
            dtype = image.dtype
            dcm = cohort.dicoms[ix]
            id_ = cohort.ids[ix]
            gt = cohort.ground_truth[ix]
            padding = dcm.data_element(
                'PixelPaddingValue').value if 'PixelPaddingValue' in dcm else None
            slope = dcm.data_element('RescaleSlope').value
            intercept = dcm.data_element('RescaleIntercept').value
            min_, max_ = float(np.min(image)), float(np.max(image))
            mmpp_x, mmpp_y = dcm.data_element('PixelSpacing').value
            assert mmpp_x == mmpp_y
            if predictions:
                pr = predictions[ix]
                row = (id_, gt, pr, dtype, min_, max_, slope, intercept, mmpp_x, padding)
            else:
                row = (id_, gt, dtype, min_, max_, slope, intercept, mmpp_x, padding)

            df.loc[ix] = row

        # display(df.describe(include='all'))
        # noinspection PyTypeChecker
        # display(df)
        if savefilename is not None:
            with open(self.results_dir + savefilename, 'w') as fp:
                df.to_html(fp)

    def show_images(self, cohort, predictions=None, savefilename=None):
        fig, axes = plt.subplots(nrows=self.show_images_nrows, ncols=self.show_images_ncols,
                                 figsize=self.show_images_figsize)
        for ix, ax in enumerate(axes.flat):  # Show just a selection
            if ix >= len(cohort.images):
                break
            im = cohort.images[ix]
            gt = cohort.ground_truth[ix]
            pltim = ax.imshow(im)
            title = "%s GT=%d" % (cohort.ids[ix], gt)
            if predictions is not None:
                title += "; Pr=%d" % predictions[ix]
            ax.set_title(title)
            fig.colorbar(pltim, ax=ax)

        if savefilename is not None:
            _, extension = os.path.splitext(savefilename)
            assert extension in ('.png', '.jpeg')
            plt.savefig(self.results_dir + savefilename, bbox_inches='tight', pad_inches=0.5)
            plt.show()
        else:
            plt.show()

    def generate_training_html(self, training_cohort, history):
        """ Create a html page in the results directory, named
        'training_results.html', along with graphical files which
        it references."""

        # Create the main (training) page
        contents = """<!DOCTYPE html>
        <html>
        <body>
        <h1>Kaggle CTMI Example: Training</h1>
        A table of training datasets can be seen <a href="training_cohort_table.html">  here </a>.
        <img src="training_images.png">
        <br>
        <img src="training_plot.png">
        <br>
        </body>
        </html>
        """
        with open(self.results_dir + 'training_results.html', 'w') as fp:
            fp.write(contents)

        # Create the graphical files
        self.show_images(training_cohort, None, 'training_images.png')
        self.explore_cohort(training_cohort, savefilename='training_cohort_table.html')
        self.plot_training_history(history, os.path.join(self.results_dir, 'training_plot.png'))

    def generate_evaluation_html(self, test_cohort, predictions):

        # Compute the accuracy score and format a sentence
        score = accuracy_score(test_cohort.ground_truth, predictions)
        result = 'Test accuracy: %5.3f' % score
        logging.info(result)

        # Create the main (evaluation) page
        contents = """<!DOCTYPE html>
        <html>
        <body>
        <h1>Kaggle CTMI Example: Evaluation</h1>
        A table of test datasets can be seen <a href="test_cohort_table.html">  here </a>.
        <img src="test_images.png">
        <br>
        %s
        <br>
        ... more results to come!
        </body>
        </html>
        """
        with open(self.results_dir + 'evaluation_results.html', 'w') as fp:
            fp.write(contents % result)

        self.show_images(test_cohort, predictions, 'test_images.png')
        self.explore_cohort(test_cohort, predictions, 'test_cohort_table.html')

    @staticmethod
    def empty_dir(dirpath):
        """Remove all files (.html and .png) in the results folder (be careful!)"""
        ll = [glob(dirpath + '/*.' + ext) for ext in ['html', 'png', 'npy']]
        files_to_delete = [j for i in ll for j in i]  # Flatten the list of lists!
        for f in files_to_delete:
            os.remove(f)

    def empty_results_dir(self):
        """Remove all files (.html and .png) in the results folder (be careful!)"""
        self.empty_dir(self.results_dir)

    def plot_training_history(self, history, save_file_path):
        epochs = range(1, len(history.acc) + 1)
        plt.figure()
        plt.plot(epochs, history.acc, label='Train')
        plt.plot(epochs, history.val_acc, label='Validation')
        plt.ylim(0.0, 1.0)
        plt.xlabel('Epochs')
        plt.ylabel('Accuracy')
        plt.legend()

        if save_file_path is not None:
            _, extension = os.path.splitext(save_file_path)
            assert extension in ('.png', '.jpeg')
            plt.savefig(save_file_path)
            plt.show()
        else:
            plt.show()

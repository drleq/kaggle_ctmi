import logging
import os
from glob import glob

import numpy as np

from kaggle_ctmi.model import Model


class AlgorithmCache(object):
    """
    Manages file based cache for preprocessing, trained models and predictions.
    Knows nothing about computation
    get_xyz() methods return None if cache not available
    set_xys() clears the cache when None is passed
    """

    def __init__(self, cache_root, model_dir=None):
        self.cache_paths = CachePaths(cache_root, model_dir)

    # Preprocessing cache
    def get_preprocessed_cache(self, id_):
        cache_file_path = os.path.join(self.cache_paths.preprocess_cache_dir, id_ + '.npy')
        if os.path.exists(cache_file_path):
            return np.load(cache_file_path)
        else:
            return None

    def set_preprocessed_cache(self, id_, im):
        cache_file_path = os.path.join(self.cache_paths.preprocess_cache_dir, id_ + '.npy')
        if os.path.exists(cache_file_path):
            os.remove(cache_file_path)
        if im is not None:
            assert im.ndim == 2
            np.save(cache_file_path, im)

    def clear_preprocess_cache(self):
        ppc_dir = self.cache_paths.preprocess_cache_dir
        if ppc_dir:
            logging.debug("Removing preprocessing cache files %s", ppc_dir)
            files_to_delete = glob(ppc_dir + '*.npy')
            for f in files_to_delete:
                os.remove(f)

    # Model cache
    def get_model_cache(self):
        model_path = os.path.join(self.cache_paths.train_cache_dir, 'model')
        if os.path.exists(model_path + '.h5'):
            assert os.path.exists(model_path + '.json')
            model = Model()
            model.load_network(model_path)
            return model
        else:
            assert not os.path.exists(model_path + '.json')
            return None

    def set_model_cache(self, model):
        model_path = os.path.join(self.cache_paths.train_cache_dir, 'model')
        if os.path.exists(model_path + '.h5'):
            os.remove(model_path + '.h5')
            os.remove(model_path + '.json')
        if model is not None:
            model.save_network(model_path)

    def clear_model_cache(self):
        self.set_model_cache(None)

    # Predictions cache
    def get_prediction_cache(self, id_):
        cache_file_path = os.path.join(self.cache_paths.predict_cache_dir, id_ + '.txt')
        if os.path.exists(cache_file_path):
            y = int(np.loadtxt(cache_file_path, np.int))
            return y
        else:
            return None

    def set_prediction_cache(self, id_, y):
        cache_file_path = os.path.join(self.cache_paths.predict_cache_dir, id_ + '.txt')
        if os.path.exists(cache_file_path):
            os.remove(cache_file_path)
        if y is not None:
            assert isinstance(y, int)
            # noinspection PyTypeChecker
            np.savetxt(cache_file_path, np.array([y]), fmt='%d')

    def clear_prediction_cache(self):
        pred_dir = self.cache_paths.predict_cache_dir
        if pred_dir:
            logging.debug("Removing prediction cache files %s", pred_dir)
            files_to_delete = glob(pred_dir + '*.txt')
            for f in files_to_delete:
                os.remove(f)


class CachePaths(object):
    """ Holds paths to directories holding cached results
    """

    def __init__(self, cache_dir_root, model_dir=None):
        self.preprocess_cache_dir = cache_dir_root + 'preprocess/'
        self.train_cache_dir = cache_dir_root + 'train/'
        self.predict_cache_dir = cache_dir_root + 'predict/'
        os.makedirs(self.preprocess_cache_dir, exist_ok=True)
        os.makedirs(self.predict_cache_dir, exist_ok=True)

        if model_dir:
            # If provided, cache models in a special location
            self.train_cache_dir = model_dir

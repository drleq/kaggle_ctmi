"""
A simple solution to CT / CTA detection based in Kaggle datasets.
"""

import argparse
import logging
import os
import sys
import time

import numpy as np

from kaggle_ctmi.algorithm import Algorithm
from kaggle_ctmi.cohort import ShaipWorkspace, Cohort
from kaggle_ctmi.results import Results


class Experiment(object):
    """ This is the top-level class, orchestrating train/test split of the cohort,
    training and evaluation.  However he details are all elsewhere"""

    def __init__(self, shaip_root_dir):
        self.shaip = ShaipWorkspace(shaip_root_dir)
        self.shaip.check()
        self.algorithm = None
        self.results = Results(self.shaip.results_dir)
        self.args = None

    def command_line(self, argv):
        parser = argparse.ArgumentParser(
            prog='experiment.py',
            description='Simple CT/CTA image discrimination to run in SHAIP',
            epilog='If a stage is not forced but is required by later stages, '
                   'cache will be used if available, else computation performed. '
                   'If no stages are specified, program does nothing - exits.')

        parser.add_argument('-P', '--preprocess',
                            help='force pre-processing from dicom to cached .npy',
                            action='store_true', default=False)

        parser.add_argument('-t', '--train',
                            help='force model training, else use saved model',
                            action='store_true', default=False)

        parser.add_argument('-p', '--predict',
                            help='force prediction of the model over the test set',
                            action='store_true', default=False)

        parser.add_argument('-e', '--evaluate',
                            help='generate results; if only this flag is given, whole pipeline '
                                 'is performed, using cache where available',
                            action='store_true', default=False)

        args = parser.parse_args(argv[1:])
        if not any([args.train, args.predict, args.evaluate, args.preprocess]):
            parser.print_help()
            sys.exit(0)
        self.args = args

    def setup_logging(self):
        # see https://docs.python.org/2.4/lib/multiple-destinations.html

        logger = logging.getLogger('')
        logger.setLevel(logging.DEBUG)

        if len(logger.handlers) <= 1:
            # avoid double setup which can happen in unit tests

            # Define a Handler which writes INFO messages or higher to the sys.stderr
            console_handler = logging.StreamHandler()
            console_handler.setLevel(logging.INFO)
            simple_formatter = logging.Formatter('%(levelname)-8s %(message)s')
            console_handler.setFormatter(simple_formatter)

            # Set up logging to file for DEBUG messages or higher
            logfile_path = os.path.join(self.shaip.results_dir, 'kaggle-ctmi.log')
            logfile_handler = logging.FileHandler(filename=logfile_path)
            logfile_handler.setLevel(logging.DEBUG)
            verbose_formatter = logging.Formatter(
                '%(asctime)s - %(levelname)s - %(message)s',
                datefmt='%d/%m/%y %H:%M')
            logfile_handler.setFormatter(verbose_formatter)

            # add the handlers to the logger
            logger.addHandler(console_handler)
            logger.addHandler(logfile_handler)

            # Silence matplotlib debug messages
            mpl_logger = logging.getLogger('matplotlib.font_manager')
            mpl_logger.setLevel(logging.WARNING)

    def setup(self, argv):
        np.random.seed(42)
        self.setup_logging()
        self.command_line(argv)
        # Start with an empty results folder
        self.results.empty_results_dir()
        self.results.generate_top_level_index_html()

    @staticmethod
    def check_for_gpus():
        from keras import backend as k
        # noinspection PyProtectedMember
        gpus = k.tensorflow_backend._get_available_gpus()
        if len(gpus) == 0:
            logging.warning("No GPUs available, running on CPU")
        else:
            logging.info("GPUs available: %d", len(gpus))

    def preprocessing_stage(self, cohort):
        """ If requested, clear cache and force preprocessing calculation"""
        if self.args.preprocess:
            self.algorithm.cache.clear_preprocess_cache()
            self.algorithm.cache.clear_model_cache()
            self.algorithm.cache.clear_prediction_cache()
            # Force the lazy computation, results saved to cache
            _ = self.algorithm.preprocessed_images(cohort)

    def training_stage(self, train_cohort):
        if self.args.train:
            # We don't actually need to pass the train cohort since it's held
            # in the Algorithm class, but it looks nicer to pass it.
            assert train_cohort is self.algorithm.train_cohort
            self.algorithm.cache.clear_model_cache()
            self.algorithm.cache.clear_prediction_cache()
            model = self.algorithm.train_model()
            assert os.path.exists(os.path.join(self.shaip.models_dir, 'model.h5'))
            self.results.generate_training_html(train_cohort, model.history)

    def prediction_stage(self, test_cohort):
        if self.args.predict:
            # Force prediction to run by invalidating cache
            self.algorithm.cache.clear_prediction_cache()
            _ = self.algorithm.predict(test_cohort)

    def evaluation_stage(self, test_cohort):
        if self.args.evaluate:
            # Note that previous processing steps will be triggered as needed
            test_predictions = self.algorithm.predict(test_cohort)
            logging.info("Generating results to shaip/workspace/output/results/index.html...")
            self.results.generate_evaluation_html(test_cohort, test_predictions)

    def main(self, argv):
        """
        Main Experiment entry point.
        argv is the full argument list, so argv[0] is the program name.
        So in production call as main(sys.argv).
        """

        # Deal with command-line arguments, logging, random seeds....
        self.setup(argv)

        logging.info("Starting Kaggle-CTMI Experiment")

        self.check_for_gpus()

        start_time = time.time()

        # Find our data from inputs and split into train and test sub-cohorts
        cohort = Cohort(self.shaip)
        train_cohort, test_cohort = cohort.split_cohort_train_test(0.3)

        self.algorithm = Algorithm(train_cohort, self.shaip.cache_dir, self.shaip.models_dir)

        # Following are the main phases of analysis.  Each of these will force
        # computation iff the corresponding command-line flag is set.   When some
        # processing requires results from a previous phase, these will be supplied
        # by lazy evaluation - read from file-based cache if available, else computed.

        self.preprocessing_stage(cohort)

        self.training_stage(train_cohort)

        self.prediction_stage(test_cohort)

        self.evaluation_stage(test_cohort)

        logging.info("Kaggle-CTMI Experiment done in %4.1f seconds.\n", (time.time() - start_time))


# Lets do it!
if __name__ == '__main__':
    expt = Experiment('workspace/')
    # You'll need to set command arguments - e.g. --train --predict --evaluate (or -tpe)
    expt.main(sys.argv)

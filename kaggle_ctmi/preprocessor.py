import numpy as np
from skimage.transform import downscale_local_mean


class Preprocessor(object):

    downsample_factor = (4, 4)
    imshape = tuple(512 // dsf for dsf in downsample_factor)  # e.g. (128, 128)

    def __init__(self):
        pass

    @staticmethod
    def preprocess_one_dicom(dcm):
        """ Return a nicely normalised numpy float32 image """
        raw_image = dcm.pixel_array

        # print(raw_image.dtype)
        slope = dcm.data_element('RescaleSlope').value
        intercept = dcm.data_element('RescaleIntercept').value

        image = np.array(raw_image, dtype=np.float32)
        image = image * slope + intercept
        image = np.array(image, dtype=np.float32)

        # It seems that padding value lies!  So we'll just clamp image values and hope for the best!
        # logging.debug("Image (min,max) = (%6.1f, %6.1f)", np.min(image), np.max(image))
        clip_min = -200.0
        clip_max = 1000.0
        image[image < clip_min] = clip_min
        image[image > clip_max] = clip_max

        assert np.min(image) >= clip_min
        assert np.max(image) <= clip_max

        # Finally, downscale !

        image = downscale_local_mean(image, Preprocessor.downsample_factor)

        return image

    @staticmethod
    def preprocessed_images(cohort):
        """ Apply preprocessing - mainly conversion to HU """
        cohort_dicoms = cohort.dicoms
        result = [Preprocessor.preprocess_one_dicom(cohort_dicoms[ix])
                  for ix in range(cohort.size)]

        return result

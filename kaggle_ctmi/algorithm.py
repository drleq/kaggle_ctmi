"""
A maximally simple solution to CT / CTA detection!
"""

import logging

from kaggle_ctmi.algorithm_cache import AlgorithmCache
from kaggle_ctmi.model import Model
from kaggle_ctmi.preprocessor import Preprocessor


# noinspection PyMethodMayBeStatic
class Algorithm(object):
    """ Links together the processing stages (preprocess, training,
     prediction) with lazy caching accessors.
    Caching is to files (in 'cache' directory), so persist between runs."""

    def __init__(self, train_cohort, cache_root, model_dir=None):
        """ cache_root (full path) which the algorithm can use
        for caching results (e.g. preprocessing) between invocations."""
        self.train_cohort = train_cohort
        self.cache = AlgorithmCache(cache_root, model_dir)
        self.history = None  # Will keep a plot of accuracy by epoch

    def preprocess_one(self, cohort, id_):
        """ Preprocess one dicom image, returning an image """
        im = self.cache.get_preprocessed_cache(id_)
        if im is None:
            logging.info('Preprocessing...')
            im = Preprocessor.preprocess_one_dicom(cohort.dicom(id_))
            self.cache.set_preprocessed_cache(id_, im)
        else:
            logging.info('(Using preprocessing cache)')
        return im

    def preprocessed_images(self, cohort):
        """ Preprocess all dicoms images in the cohort """

        # Trick to ensure we only show a logging message once.
        dup_filter = DuplicateFilter()
        logging.getLogger().addFilter(dup_filter)

        result = [self.preprocess_one(cohort, id_) for id_ in cohort.ids]

        logging.getLogger().removeFilter(dup_filter)
        return result

    def predict(self, cohort):
        """ Perform prediction on whole given test set.
        Model training and preprocessing are lazily triggered
        if / as needed.   Returns a list of ints - e.g. [1,0,0,1,1...]"""

        model = None

        dup_filter = DuplicateFilter()
        logging.getLogger().addFilter(dup_filter)

        predictions = []
        for id_ in cohort.ids:
            y = self.cache.get_prediction_cache(id_)
            if y is None:
                logging.info("Predicting...")
                if model is None:
                    # Don't want to fetch model from cache multiple times
                    model = self.train_model()
                y = model.predict_one(self.preprocess_one(cohort, id_))
                self.cache.set_prediction_cache(id_, y)
            else:
                logging.info("(Using prediction cache)")
            predictions.extend([y])
        assert len(predictions) == cohort.size

        logging.getLogger().removeFilter(dup_filter)

        return predictions

    def train_model(self):
        """ Train model on training set.  Preprocessing is lazily triggered
        if necessary """
        model = self.cache.get_model_cache()
        if not model:
            logging.info("Training model...")
            model = Model()
            model.train(
                self.preprocessed_images(self.train_cohort),
                self.train_cohort.ground_truth)
            self.cache.set_model_cache(model)
        else:
            logging.info('(Using saved model)')
        return model


class DuplicateFilter(object):
    """ A logging filter to remove duplicates.  (Used in preprocessing method)"""

    def __init__(self):
        self.msgs = set()

    def filter(self, record):
        rv = record.msg not in self.msgs
        self.msgs.add(record.msg)
        return rv

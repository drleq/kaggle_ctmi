"""
A maximally simple solution to CT / CTA detection!
"""

import logging
import os

import keras
import numpy as np
from keras.layers import Conv2D, MaxPooling2D, Flatten, Dense
from keras.models import Sequential, model_from_json
from keras.preprocessing.image import ImageDataGenerator
from sklearn.model_selection import train_test_split

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # Suppress tensorflow warnings


# noinspection PyMethodMayBeStatic


class Model(object):
    """ This contains the details of our solution, intended to be largely
    isolated from other infra-structure issues.  At this level we perform no caching. """

    def __init__(self, image_shape=(128, 128)):
        self.image_shape = image_shape
        self.history = None  # Will keep a plot of accuracy by epoch
        self.network = None  # The network we will train (fit)

    # Class level constants

    epochs = 35             # (unit tests will @patch reduce this for speed)

    def predict_one(self, preprocessed_image):
        assert preprocessed_image.ndim == 2
        result = self.network.predict_classes(self._data_scaling_one_image(preprocessed_image))
        return int(result)

    def predict(self, preprocessed_images):
        result = [self.predict_one(im) for im in preprocessed_images]
        return result

    def _build_network(self):
        input_shape = self.image_shape + (1,)
        model = Sequential()
        model.add(Conv2D(8, kernel_size=(3, 3), strides=(1, 1), activation='relu',
                         input_shape=input_shape))
        model.add(MaxPooling2D(pool_size=(3, 3)))
        model.add(Conv2D(8, (3, 3), strides=(1, 1), activation='relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Flatten())
        model.add(Dense(10, activation='relu'))
        model.add(Dense(10, activation='relu'))
        model.add(Dense(2, activation='softmax'))

        return model

    def _data_scaling(self, images):
        """
        Given a list of pre-processed images (e.g. from PreprocessedCohort.images) perform
        intensity scaling and reshaping, returning a 4D tensor (n, x, y, 1) ready for feeding
        to a network
        """

        scaled_images = [self._data_scaling_one_image(im) for im in images]

        return np.squeeze(np.array(scaled_images), axis=1)

    def _data_scaling_one_image(self, image):
        """
        Given a single pre-processed images perform
        intensity scaling and reshaping, returning a list of 4D tensors (1, x, y, 1) ready
        for feeding to a network model.
        """
        siz = image.shape
        x_data = np.array(image).reshape(1, siz[0], siz[1], 1)
        x_data = x_data.astype(np.float32)
        x_data = (x_data + 100) / 150.0

        return x_data

    def train(self, preprocessed_images, ground_truth):
        x_data = self._data_scaling(preprocessed_images)
        y_data = keras.utils.to_categorical(ground_truth, 2)

        self.fit(x_data, y_data)

    def fit(self, x_data, y_data):
        # Split into train and validation sets
        # (Note that the train/test split has already been done at the cohort level)
        val_split = 0.2
        x_train, x_val, y_train, y_val = \
            train_test_split(x_data, y_data,
                             stratify=y_data,
                             test_size=val_split, shuffle=True, random_state=43)

        # Augmentation.  The ImageDataGenerator specifies what augmentation transformations
        # are applied.
        datagen = ImageDataGenerator(
            rotation_range=30,
            width_shift_range=0.1, height_shift_range=0.2,
            horizontal_flip=True, vertical_flip=False,
            zoom_range=0.3)

        # Build and compile the network
        network = self._build_network()
        network.compile(
            loss=keras.losses.categorical_crossentropy,
            optimizer=keras.optimizers.Adam(),
            metrics=['accuracy'])
        self.history = AccuracyHistory()

        # Train and save the network
        batch_size = min(20, len(x_data))
        steps_per_epoch = len(x_data) / batch_size

        # We draw training and validation flows from entirely separate datasets
        network.fit_generator(generator=datagen.flow(x_train, y_train, batch_size=batch_size),
                              validation_data=datagen.flow(x_val, y_val, batch_size=len(x_val)),
                              validation_steps=1,
                              steps_per_epoch=steps_per_epoch,
                              epochs=self.epochs,
                              verbose=0,
                              callbacks=[self.history])
        network.summary(print_fn=logging.debug)
        self.network = network

    def save_network(self, fname):
        """ Save model and wieghts to fname and fname.h5 files respectively
        fname can include a directory which will be created if it doesn't exist"""

        directory = os.path.dirname(fname)
        if directory and not os.path.isdir(directory):
            logging.warning("Creating directory %s" % directory)
            os.makedirs(directory)

        model = self.network
        model_json = model.to_json()
        with open(fname + '.json', 'w') as json_file:
            json_file.write(model_json)
        model.save_weights(fname + '.h5')
        logging.info("Model saved to %s[.json,.h5] files", fname)

    def load_network(self, fname):
        """ Load a model from fname.json and fname.h5, and return it.
        (Note that the loaded model must be compiled before use)"""
        # load json and create model
        json_file = open(fname + '.json', 'r')
        loaded_model_json = json_file.read()
        json_file.close()
        loaded_model = model_from_json(loaded_model_json)
        # load weights into new model
        loaded_model.load_weights(fname + '.h5')
        logging.info("Model loaded from %s[.json,.h5] files", fname)
        self.network = loaded_model


class AccuracyHistory(keras.callbacks.Callback):
    """ Record and plot training progress """

    def __init__(self):
        super().__init__()
        self.acc = []
        self.val_acc = []

    def on_train_begin(self, logs=None):
        self.acc = []
        self.val_acc = []

    def on_epoch_end(self, epoch, logs=None):
        self.acc.append(logs.get('acc'))
        self.val_acc.append(logs.get('val_acc'))


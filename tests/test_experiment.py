import logging
import os
import shutil
from unittest import TestCase
from tempfile import TemporaryDirectory
from unittest.mock import patch

from kaggle_ctmi.cohort import ShaipWorkspace
from kaggle_ctmi.experiment import Experiment

unit_test_workspace = 'unittest/workspace/'


def test_logging():
    print()
    expt = Experiment(unit_test_workspace)
    expt.setup_logging()

    logging.info("Starting Kaggle-CTMI Experiment")
    logging.info("This should go to both the console and logfile")
    logging.debug("This should go to the logfile only")
    logging.warning("You have been warned!")


class TestCommandLine(TestCase):

    def test_command_line_empty(self):
        expt = Experiment(unit_test_workspace)
        with self.assertRaises(SystemExit):
            # With no arguments there's nothing to do, so expect to print usage then exit
            expt.command_line(['experiment.py'])

    def test_command_line_train_only(self):
        expt = Experiment(unit_test_workspace)
        expt.command_line(['experiment.py', '--train'])
        self.assertEqual(expt.args.train, True)
        self.assertEqual(expt.args.predict, False)
        self.assertEqual(expt.args.evaluate, False)

    def test_command_line_all_phases(self):
        expt = Experiment(unit_test_workspace)
        expt.command_line(['experiment.py', '-tpe'])
        self.assertEqual(expt.args.train, True)
        self.assertEqual(expt.args.predict, True)
        self.assertEqual(expt.args.evaluate, True)

    def test_command_line_h(self):
        expt = Experiment(unit_test_workspace)
        with self.assertRaises(SystemExit):
            expt.command_line(['experiment.py', '-h'])

    def test_command_line_help(self):
        expt = Experiment(unit_test_workspace)
        with self.assertRaises(SystemExit):
            expt.command_line(['experiment.py', '--help'])

    def test_command_line_error(self):
        expt = Experiment(unit_test_workspace)
        with self.assertRaises(SystemExit):
            expt.command_line(['experiment.py', '--sillyoption'])


# The following patching is to speed up the tests.
@patch('kaggle_ctmi.model.Model.epochs', 2)
@patch('kaggle_ctmi.results.Results.show_images_figsize', (2, 4))
@patch('kaggle_ctmi.results.Results.show_images_nrows', 1)
@patch('kaggle_ctmi.results.Results.show_images_ncols', 2)
class TestExperimentMain(TestCase):

    # noinspection PyMethodMayBeStatic
    def copy_workspace_to(self, tmp_shaip_path):
        """ Helper to copy the unittest version of the workspace
        (inputs only) to a given temporary directory.  Thus we
        can insure that each test sees a clean copy and do not
        interfere.  Return the path of the workspace"""
        shaip = ShaipWorkspace(unit_test_workspace)
        shutil.copytree(shaip.data_dir,
                        os.path.join(tmp_shaip_path, 'ws/input/dicom'))
        shutil.copytree(shaip.groundtruth_dir,
                        os.path.join(tmp_shaip_path, 'ws/input/ground_truth'))
        return os.path.join(tmp_shaip_path, 'ws/')

    def test_all_phases(self):
        with TemporaryDirectory() as tmp_shaip_path:
            ws_path = self.copy_workspace_to(tmp_shaip_path)
            expt = Experiment(ws_path)
            expt.main(['experiment.py', '-Ptpe'])

    def test_preprocess_only(self):
        with TemporaryDirectory() as tmp_shaip_path:
            ws_path = self.copy_workspace_to(tmp_shaip_path)
            expt = Experiment(ws_path)
            expt.main(['experiment.py', '-P'])

    def test_train_only(self):
        expt = Experiment(unit_test_workspace)
        expt.main(['experiment.py', '-t'])

    def test_predict_only(self):
        with TemporaryDirectory() as tmp_shaip_path:
            ws_path = self.copy_workspace_to(tmp_shaip_path)
            expt = Experiment(ws_path)
            expt.main(['experiment.py', '-p'])

    def test_evaluation_only(self):
        with TemporaryDirectory() as tmp_shaip_path:
            ws_path = self.copy_workspace_to(tmp_shaip_path)
            expt = Experiment(ws_path)
            expt.main(['experiment.py', '-e'])

    def test_evaluation_preprocess_and_prediction(self):
        with TemporaryDirectory() as tmp_shaip_path:
            ws_path = self.copy_workspace_to(tmp_shaip_path)
            expt = Experiment(ws_path)
            expt.main(['experiment.py', '-Pp'])

    def test_nothing(self):
        with TemporaryDirectory() as tmp_shaip_path:
            ws_path = self.copy_workspace_to(tmp_shaip_path)
            expt = Experiment(ws_path)
            with self.assertRaises(SystemExit):
                expt.main(['experiment.py'])

    def test_h(self):
        with TemporaryDirectory() as tmp_shaip_path:
            ws_path = self.copy_workspace_to(tmp_shaip_path)
            print()
            expt = Experiment(ws_path)
            with self.assertRaises(SystemExit):
                expt.main(['experiment.py', '-h'])

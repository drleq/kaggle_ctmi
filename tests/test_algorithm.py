import unittest
from unittest.mock import patch

import matplotlib
import numpy as np

from kaggle_ctmi.algorithm import Algorithm
from kaggle_ctmi.cohort import Cohort, ShaipWorkspace

# Set the 'Agg' matplotlib backend to avoid plots appearing on the display (we only want them
# saved to .png files)
matplotlib.use('Agg')  # Comment out this line to see plotting results


# noinspection PyPep8


@patch('kaggle_ctmi.model.Model.epochs', 2)
class TestAlgorithm(unittest.TestCase):
    def setUp(self):
        self.shaip = ShaipWorkspace()
        self.cohort = Cohort(self.shaip)
        self.train_cohort, self.test_cohort = self.cohort.split_cohort_train_test()

    def test_preprocess_one(self):
        algorithm = Algorithm(self.train_cohort, self.shaip.cache_dir)
        one_id = self.train_cohort.ids[0]
        pp_im = algorithm.preprocess_one(self.train_cohort, one_id)
        assert pp_im.shape == (128, 128)

    def test_preprocessed_images(self):
        algorithm = Algorithm(self.train_cohort, self.shaip.cache_dir)

        # Test for training set, without then with cache
        algorithm.cache.clear_preprocess_cache()
        result1 = algorithm.preprocessed_images(self.train_cohort)
        assert len(result1) == self.train_cohort.size

        result2 = algorithm.preprocessed_images(self.train_cohort)
        assert len(result2) == self.train_cohort.size
        assert np.array_equal(result1[0], result2[0])

    def test_train(self):
        algorithm = Algorithm(self.train_cohort, self.shaip.cache_dir)
        algorithm.cache.clear_model_cache()
        model = algorithm.train_model()
        assert model.network is not None
        assert algorithm.cache.get_model_cache() is not None

        model = algorithm.train_model()
        assert model.network is not None

    def test_predict(self):
        algorithm = Algorithm(self.train_cohort, self.shaip.cache_dir)
        one_id = self.test_cohort.ids[0]
        algorithm.cache.clear_prediction_cache()
        assert algorithm.cache.get_prediction_cache(one_id) is None
        predictions = algorithm.predict(self.test_cohort)
        assert len(predictions) == self.test_cohort.size
        assert algorithm.cache.get_prediction_cache(one_id) is not None

        predictions = algorithm.predict(self.test_cohort)
        assert len(predictions) == self.test_cohort.size

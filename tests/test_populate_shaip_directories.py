import unittest
from glob import glob
from tempfile import TemporaryDirectory
import shaip_creation.populate_shaip_directories as psd


# noinspection PyMethodMayBeStatic
class ShaipCreationTestCase(unittest.TestCase):
    def test_filename_to_id(self):
        filename = "ID_0087_AGE_0044_CONTRAST_0_CT.dcm"
        id_ = psd._filename_to_id(filename)
        assert id_ == "00087"

    def test_filename_to_gt(self):
        filename0 = "ID_0087_AGE_0044_CONTRAST_0_CT.dcm"
        gt0 = psd._filename_to_contrast_gtstring(filename0)
        assert gt0 == 'ct'

        filename1 = "ID_0087_AGE_0044_CONTRAST_1_CT.dcm"
        gt1 = psd._filename_to_contrast_gtstring(filename1)
        assert gt1 == 'cta'

    def test_do_it(self):
        test_ids = ['0000' + str(i) for i in range(2)]
        with TemporaryDirectory() as rootdir:
            psd.do_it(rootdir, test_ids)
            files = glob(rootdir + '/**/*', recursive=True)
            print()
            for f in files:
                print(f)


if __name__ == '__main__':
    unittest.main()

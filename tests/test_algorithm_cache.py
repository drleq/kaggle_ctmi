import unittest
from tempfile import TemporaryDirectory

import numpy as np
from keras import Sequential
from keras.layers import Dense

from kaggle_ctmi.algorithm_cache import AlgorithmCache
from kaggle_ctmi.model import Model


class TestAlgorithmCache(unittest.TestCase):

    def test_preprocess_cache(self):
        with TemporaryDirectory() as cache_root:
            cache = AlgorithmCache(cache_root)
            result = cache.get_preprocessed_cache('0001')
            assert result is None

            im = np.random.random((10, 10))
            cache.set_preprocessed_cache('0001', im)

            result = cache.get_preprocessed_cache('0001')
            assert np.array_equal(im, result)

            cache.set_preprocessed_cache('0001', None)
            result = cache.get_preprocessed_cache('0001')
            assert result is None

            cache.set_preprocessed_cache('0001', im)
            cache.set_preprocessed_cache('0002', im)
            cache.clear_preprocess_cache()
            r_0001 = cache.get_preprocessed_cache('0001')
            r_0002 = cache.get_preprocessed_cache('0002')
            assert r_0001 is None and r_0002 is None

    def test_model_cache(self):
        with TemporaryDirectory() as cache_root:
            exp_cache = AlgorithmCache(cache_root)
            model = exp_cache.get_model_cache()
            assert model is None

            model = Model()
            network = Sequential()
            network.add(Dense(10, activation='relu', input_shape=(5, 1)))
            model.network = network
            exp_cache.set_model_cache(model)

            result = exp_cache.get_model_cache()
            assert model.network.count_params() == result.network.count_params()

            exp_cache.set_model_cache(None)
            result = exp_cache.get_model_cache()
            assert result is None

    def test_prediction_cache(self):
        with TemporaryDirectory() as cache_root:
            exp_cache = AlgorithmCache(cache_root)
            result = exp_cache.get_prediction_cache('0001')
            assert result is None

            exp_cache.set_prediction_cache('0001', 1)

            y = exp_cache.get_prediction_cache('0001')
            assert y == 1

            exp_cache.set_prediction_cache('0001', None)
            result = exp_cache.get_prediction_cache('0001')
            assert result is None

            exp_cache.set_prediction_cache('0001', 1)
            exp_cache.set_prediction_cache('0002', 1)
            exp_cache.clear_prediction_cache()
            r_0001 = exp_cache.get_prediction_cache('0001')
            r_0002 = exp_cache.get_prediction_cache('0002')
            assert r_0001 is None and r_0002 is None


import unittest

import numpy as np

from kaggle_ctmi.cohort import Cohort, ShaipWorkspace


class TestShaipWorkspace(unittest.TestCase):
    def setUp(self):
        self.shaip = ShaipWorkspace()

    def test_dicom_path_from_id(self):
        path = self.shaip.dicom_path_from_id('00012')
        print(path)
        assert path == 'unittest/workspace/input/dicom/study00012/series00000/00012.dcm'

    def test_gt_path_from_id(self):
        path = self.shaip.gt_path_from_id('00012')
        assert path == 'unittest/workspace/input/ground_truth/study00012/series00000/00012.txt'

    def test_id_from_path(self):
        path = 'unittest/workspace/input/ground_truth/study00042/series00000/1234.txt'
        id_ = self.shaip.id_from_path(path)
        print(id_)
        assert id_ == '00042'


class TestCohort(unittest.TestCase):
    def setUp(self):
        self.shaip = ShaipWorkspace()
        self.cohort = Cohort(self.shaip)

    def test_init(self):
        assert len(self.cohort.ids) == 16
        assert len(self.cohort.ids[0]) == 5

    def test_read_contrast_gt(self):
        gt_path = 'unittest/workspace/input/ground_truth/study00001/series00000/00001.txt'
        gt = self.cohort._read_contrast_gt(gt_path)
        assert gt == 1

    # noinspection PyMethodMayBeStatic
    def cohort_accessors_test_helper(self, cohort):
        assert len(cohort.dicoms) == len(cohort.ids) == len(cohort.images) == len(
            cohort.ground_truth) == len(cohort.ground_truth) == cohort.size
        assert all(['PixelData' in dcm for dcm in cohort.dicoms])
        assert len(cohort.images) == len(cohort.ids)
        assert all([im.shape == (512, 512) for im in cohort.images])
        assert all([im.dtype in (np.int16, np.uint16) for im in cohort.images])
        assert all([gt in (0, 1) for gt in cohort.ground_truth])
        c0, c1 = cohort.class_counts()
        assert c0 + c1 == cohort.size

    def test_cohort_accessors(self):
        self.cohort_accessors_test_helper(self.cohort)

    def test_split_cohort_train_test(self):
        test_prop = 0.25
        train_cohort, test_cohort = self.cohort.split_cohort_train_test(test_prop)
        n = self.cohort.size
        n_train = int(n * (1.0 - test_prop))
        n_test = int(n * test_prop)
        assert n_train + n_test == n
        assert train_cohort.size == n_train
        assert test_cohort.size == n_test

        self.cohort_accessors_test_helper(train_cohort)
        self.cohort_accessors_test_helper(test_cohort)

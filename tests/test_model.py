import os
import unittest
from tempfile import TemporaryDirectory
from unittest.mock import patch

import numpy as np
from keras import Sequential
from keras.layers import Dense

from kaggle_ctmi.model import Model, AccuracyHistory


# noinspection PyMethodMayBeStatic
@patch('kaggle_ctmi.model.Model.epochs', 2)
class TestModel(unittest.TestCase):
    def test_data_scaling(self):
        xs, ys = 64, 128
        model = Model((xs, ys))
        im = np.random.uniform(size=(xs, ys), high=2000, low=-300)
        n = 3
        images = [im] * n  # test set of just 3 images
        x_data = model._data_scaling(images)
        expected_shape = (n, xs, ys, 1)
        assert x_data.shape == expected_shape
        assert x_data.dtype == np.float32

    def test_data_scaling_one_image(self):
        xs, ys = 64, 128
        model = Model((xs, ys))
        im = np.random.uniform(size=(xs, ys), high=2000, low=-300)
        x_data = model._data_scaling_one_image(im)
        expected_shape = (1, xs, ys, 1)
        assert x_data.shape == expected_shape
        assert x_data.dtype == np.float32

    def test_build_network(self):
        input_shape = (128, 128)
        model = Model(input_shape)
        network = model._build_network()
        network.summary()

    def helper_train_model(self):
        imsize = (128, 128)
        model = Model(imsize)

        # Create some random images
        a, b = -200.0, 1000.0
        im = (b - a) * np.random.random(imsize) + a
        train_images = [im] * 50
        gt = [0, 1] * (50 // 2)

        model.train(train_images, gt)

        return model, train_images

    def test_train(self):
        model, _ = self.helper_train_model()
        assert model.network is not None

    def test_predict(self):
        model, images = self.helper_train_model()

        predictions1 = model.predict(images)
        assert len(predictions1) == len(images)
        assert all(np.isscalar(y) for y in predictions1)
        assert all(y in [0, 1] for y in predictions1)

    def test_model_save_and_load(self):
        model = Model((128, 128))
        network = Sequential()
        network.add(Dense(10, activation='relu', input_shape=(5, 1)))
        model.network = network
        with TemporaryDirectory() as dir_name:
            temp_file_path = os.path.join(dir_name, 'test_model')
            model.save_network(temp_file_path)
            model.network = None
            model.load_network(temp_file_path)
            assert model.network

    def test_accuracyhistory(self):
        history = AccuracyHistory()

        # Simulate some training!
        history.on_train_begin()
        for epoch, acc, val_acc in zip([1, 2, 3, 4], [.6, .7, .75, .75], [.6, .65, .68, .65]):
            log = {'acc': acc, 'val_acc': val_acc}
            history.on_epoch_end(epoch, log)

import unittest

import matplotlib

from kaggle_ctmi.cohort import Cohort, ShaipWorkspace
from kaggle_ctmi.preprocessor import Preprocessor

# Set the 'Agg' matplotlib backend to avoid plots appearing on the display (we only want them
# saved to .png files)
matplotlib.use('Agg')      # Comment out this line to see plotting results
# noinspection PyPep8
import matplotlib.pyplot as plt


class TestPreprocessor(unittest.TestCase):
    def test__preprocess_one_dicom(self):
        cohort = Cohort(ShaipWorkspace())
        dcm1 = cohort.dicoms[0]
        image = Preprocessor.preprocess_one_dicom(dcm1)
        assert image.shape == Preprocessor.imshape
        plt.imshow(image)
        plt.colorbar()
        plt.show()

    def test_preprocessed_cohort(self):
        cohort = Cohort(ShaipWorkspace())
        ppimages = Preprocessor.preprocessed_images(cohort)
        assert len(ppimages) == cohort.size


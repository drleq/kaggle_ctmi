
import os
from tempfile import TemporaryDirectory
from unittest import TestCase
from unittest.mock import patch

from kaggle_ctmi.model import AccuracyHistory
from kaggle_ctmi.cohort import Cohort, ShaipWorkspace
from kaggle_ctmi.results import Results


# Patching figure creation to make test go much faster
# This results in poor looking figures; disable these patches if debugging figure details.
@patch('kaggle_ctmi.results.Results.show_images_figsize', (2, 4))
@patch('kaggle_ctmi.results.Results.show_images_nrows', 1)
@patch('kaggle_ctmi.results.Results.show_images_ncols', 2)
class TestResults(TestCase):
    def setUp(self):
        self.shaip = ShaipWorkspace()
        self.cohort = Cohort(self.shaip)

    def test_generate_top_level_index_html(self):
        results = Results(self.shaip.results_dir)
        results.generate_top_level_index_html()
        assert os.path.exists(self.shaip.results_dir + 'index.html')

    def test_explore_cohort(self):
        with TemporaryDirectory() as tmp_dir:
            results = Results(tmp_dir)
            savefilename = 'cohort_table.png'
            results.explore_cohort(self.cohort, savefilename=savefilename)
            assert os.path.exists(tmp_dir + savefilename)

    def test_explore_cohort_with_predictions(self):
        with TemporaryDirectory() as tmp_dir:
            results = Results(tmp_dir)
            predictions = [1] * self.cohort.size
            savefilename = 'cohort_table.png'
            results.explore_cohort(self.cohort, predictions=predictions, savefilename=savefilename)
            assert os.path.exists(tmp_dir + savefilename)

    def test_show_images(self):
        with TemporaryDirectory() as tmp_dir:
            results = Results(tmp_dir)
            predictions = [0] * self.cohort.size
            savefilename = 'image_gallery.png'
            results.show_images(self.cohort, predictions, savefilename)
            assert os.path.exists(tmp_dir + savefilename)

    def test_generate_training_html(self):
        shaip = ShaipWorkspace()
        train_cohort = self.cohort

        history = AccuracyHistory()
        history.acc = [0.5, 0.7, 0.75]
        history.val_acc = [0.5, 0.6, 0.55]

        results = Results(shaip.results_dir)
        results.generate_training_html(train_cohort, history)

    def test_generate_evaluation_html(self):
        shaip = ShaipWorkspace()

        test_cohort = self.cohort
        predictions = [0] * test_cohort.size

        results = Results(shaip.results_dir)
        results.generate_evaluation_html(test_cohort, predictions)

    def test_empty_results_dir(self):
        with TemporaryDirectory() as tmp_dir:
            results = Results(tmp_dir)
            os.mknod(os.path.join(tmp_dir, 'gash.png'))
            os.mknod(os.path.join(tmp_dir, 'gash.html'))
            os.mknod(os.path.join(tmp_dir, 'gash.py'))
            results.empty_results_dir()
            assert os.path.exists(os.path.join(tmp_dir, 'gash.py'))
            assert not os.path.exists(os.path.join(tmp_dir, 'gash.html'))
            assert not os.path.exists(os.path.join(tmp_dir, 'gash.png'))

# kaggle_ctmi
Working with DICOM images from the [Kaggle CT Medical Images](https://www.kaggle.com/kmader/siim-medical-images).

This is one of the simplest DICOM based datasets I could find. I'm taking the goal to be prediction of CT vs CTA.
My goal is to create a simple clean, short solution with a minimum of added 'infrastructure'

## Recent improvements (in forward chronological order)
* Images and GT read from directory structure as intended
* Logging to console and log file (using standard `logging` library)
* Basic dockerfile, image pushed to Docker Hub
* Lazy caching of pre-processed images as .npy files
* Command-line parameters to enable/disable experiment phases (using standard `argparse` library).
* Populate_shaip_directories script added
* Much faster unit test
* Adopted SHAIP approved workspace structure, including series
* Added run script
* Docker image now runs out of box

## Installation (updated 15/11/18)

Assuming a basic python 3 installation, roughly:
* git clone into directory `kaggle_ctmi/`
* `$ cd kaggle.ctmi`
* Setup, activate and provision a local virtual environment
  * `$ pip install --user virtualenv`
  * `$ virtualenv -p python3.5 venv`
  * `$ source venv/bin/activate`
  * `(venv)$ pip install -r requirments.txt` 

* Download and extract `dicom_dir` from 
  `https://www.kaggle.com/kmader/siim-medical-images/version/6#dicom_dir.zip`

* Run unit tests, ~40 tests should all pass.  (Know benign issue with _del on exit)
  * `(venv)$ export PYTHONPATH=.`
  * `$(venv) py.test`
  
* Setup the 'real'  'ShaipWorkspace':
  * `python shaip_creation/populate_shaip_directories.py`
  
* Run the main experiment to see help:
  * `./run` --help
  
* Run the main experiment for real (via script)
  * `./run -Ptpe`
  
* To see results point a browser at `shaip/workspace/output/results/index.html`

## Usage
```
usage: experiment.py [-h] [-P] [-t] [-p] [-e]

Simple CT/CTA image discrimination to run in SHAIP

optional arguments:
  -h, --help        show this help message and exit
  -P, --preprocess  force pre-processing from dicom to cached .npy
  -t, --train       force model training, else use saved model
  -p, --predict     force prediction of the model over the test set
  -e, --evaluate    generate results; if only this flag is given, whole
                    pipeline is performed, using cache where available

If a stage is not forced but is required by later stages, cache will be used
if available, else computation performed. If no stages are specified, program
does nothing - exits.

```

## Note on repository host

As of 7/11/18 the repository has been moved from github to bitbucket, in order to make it private.


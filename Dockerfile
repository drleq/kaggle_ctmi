FROM python:3.5.6

MAINTAINER Ian Poole

RUN groupadd shaip
RUN useradd -d /home/shaip -ms /bin/bash -g shaip -G sudo -p shaip shaip
COPY --chown=shaip:shaip requirements.txt /home/shaip/requirements.txt
RUN pip install -r /home/shaip/requirements.txt
RUN mkdir -p /home/shaip/workspace/output/results

RUN chown -R shaip:shaip /home/shaip/workspace

USER shaip
WORKDIR /home/shaip

COPY --chown=shaip:shaip kaggle_ctmi /home/shaip/kaggle_ctmi
COPY --chown=shaip:shaip tests /home/shaip/tests
COPY --chown=shaip:shaip shaip_creation /home/shaip/shaip_creation
COPY --chown=shaip:shaip unittest/ /home/shaip/unittest/
# Following is to allow out-of-box testing - typically will remap
COPY --chown=shaip:shaip workspace/input /home/shaip/workspace/input

COPY --chown=shaip:shaip run /home/shaip/run

CMD ./run --preprocess --train --predict --evaluate


